// index.js
// Cyan Villarin
// iOS Developer
// Stratpoint Technologies Inc.

'use strict'

// import needed libraries
const express = require('express')
const bodyparser = require('body-parser')
const pg = require('pg')

const app = express()

const local_config = {
    host: 'localhost',
    user: 'cyanvillarin',
    database: 'sp-sample-api-project',
    password: 'bnbn',
    port: 5432
};

const remote_config = {
    host: 'ec2-54-235-208-103.compute-1.amazonaws.com',
    user: 'jpxxaxlbhlcycb',
    database: 'd87ckj69vriodd',
    password: '2e5aea0edde73533513af6fb857d09d14b2462165c117fc154f42fcbfebbd0b7',
    port: 5432
};

const pool = new pg.Pool(remote_config);

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:true}))

// get all contacts
app.get('/api/contacts', function (req, res, next) {
    pool.connect(function (err, client, done) {
        if (err) {
            return next(err)
        }
        client.query('SELECT * FROM contacts ORDER BY _id ASC;', [], function (err, result) {
            done()

            if (err) {
                return next(err)
            }
            res.json(result.rows)
        })
    })
})

// add a new contact
app.post('/api/contacts', function (req, res, next) {
    pool.connect(function (err, client, done) {
        if (err) {
            return next(err)
        }

        const { first_name, 
            last_name, 
            address, 
            email_address, 
            contact_number 
        } = req.body

        client.query("INSERT INTO contacts (first_name, last_name, address, email_address, contact_number) VALUES ($1, $2, $3, $4, $5);", [first_name, last_name, address, email_address, contact_number], function (err, result) {
            done()
            if (err) {
                return next(err)
            }
            res.sendStatus(200)
        })
    })
})

// find contact by id
app.get('/api/contacts/:id', function (req, res, next) {
    pool.connect(function (err, client, done) {
        if (err) {
            return next(err)
        }

        const id = parseInt(req.params.id)

        client.query('SELECT * FROM contacts WHERE _id = $1;', [id], function (err, result) {
            done()
            if (err) {
                return next(err)
            }
            res.json(result.rows)
        })
    })
})

// update contact
app.put('/api/contacts/:id', function (req, res, next) {
    pool.connect(function (err, client, done) {
        if (err) {
            return next(err)
        }

        const id = parseInt(req.params.id)

        const { first_name, 
            last_name, 
            address, 
            email_address, 
            contact_number 
        } = req.body

        client.query('UPDATE contacts SET first_name = $1, last_name = $2, address = $3, email_address = $4, contact_number = $5 WHERE _id = $6;', [first_name, last_name, address, email_address, contact_number, id], function (err, result) {
            done()
            if (err) {
                return next(err)
            }
            res.json(`User modified with ID: ${id}`)
        })
    })
})

// delete contact by id
app.delete('/api/contacts/:id', function (req, res, next) {
    pool.connect(function (err, client, done) {
        if (err) {
            return next(err)
        }

        const id = parseInt(req.params.id)

        client.query('DELETE FROM contacts WHERE _id = $1;', [id], function (err, result) {
            done()
            if (err) {
                return next(err)
            }
            res.json(`User deleted with ID: ${id}`)
        })
    })
})


// app.listen(3000)
app.listen(process.env.PORT || 3000)